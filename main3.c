#include <dpu_management.h>
#include <dpu.h>
#include <dpu_rank.h>
#include <dpu_program.h>
#include <stdio.h>
#include <ufi/ufi_memory.h>
#include <dpu_memory.h>
#define DPU_BINARY "./dpu"

int a[] = {2,3,5,7,11,13,17,19};

uint32_t get_dpu_program() {
  return 0;
}

uint32_t get_dpu_symbol_offset_wram(char* symbol, struct dpu_t* dpu) {
  struct dpu_program_t *program = dpu_get_program(dpu);
  struct dpu_symbol_t symbol_t;
  printf("program pointer: %p\n", program);
  char* name = "a";
  dpu_get_symbol(program, name, &symbol_t);
  printf("address: %p\n", symbol_t.address);
  printf("length: %x\n", symbol_t.size);
  return symbol_t.address;
}

int main() {
  struct dpu_set_t set, dpu;
  dpu_alloc(1,NULL, &set);

  dpu_load(set, DPU_BINARY, NULL);
  
  uint32_t offset ;

  DPU_FOREACH(set, dpu) {
    offset = get_dpu_symbol_offset_wram("a", dpu.dpu);
    printf("address: %p\n", offset);
  }

  DPU_FOREACH(set, dpu) {
    struct dpu_rank_t *rank_t = dpu_get_rank(dpu.dpu);
    DPU_ASSERT(dpu_broadcast_to(dpu, "a", 0, a, 8, DPU_XFER_DEFAULT));
  }

  dpu_launch(set, DPU_SYNCHRONOUS);

  DPU_FOREACH(set, dpu) {
    dpu_log_read(dpu, stdout);
  }
  dpu_free(set);

  return 0;
}
