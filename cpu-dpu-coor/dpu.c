#include <stdio.h>
#include <stdint.h>

#define BUF_SIZE 1024

__mram uint64_t buffer[BUF_SIZE];

int main() {
  for(size_t i = 0; i < BUF_SIZE - 1; ){
    if(buffer[i] < buffer[i+1]) {
      printf("number is %lu\n", buffer[i]);
      i++;
    }
  }
}
