#include <dpu.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#define  DPU_BINARY "dpu"
#define BUF_SIZE 1024

uint64_t buffer[BUF_SIZE];

typedef struct _set_dpu {
  struct dpu_set_t* a1;
  struct dpu_set_t* a2;
} set_dpu;


void* write_to_dpu(void *vargp) {
   set_dpu* arg = (set_dpu*)vargp;
   struct dpu_set_t set = *(arg->a1);
   struct dpu_set_t dpu = *(arg->a2); 
    DPU_FOREACH(set, dpu) {
    for(size_t i = 0; i < 1024; i++){
      dpu_broadcast_to(dpu, "buffer", 0, buffer + i, 8, DPU_XFER_ASYNC);
      printf("CPU write to mram: %lu\n", *(buffer + i));
    }
  }
  return NULL;
}

int main() {
  for(int i = 0;i < 1024; i++) {
    buffer[i] = i;
  }
  struct dpu_set_t set, dpu;
  dpu_alloc(1, NULL, &set);
  dpu_load(set, DPU_BINARY, NULL);
  dpu_launch(set, DPU_ASYNCHRONOUS);
  pthread_t pid;
  set_dpu a = { &set, &dpu };
  pthread_create(&pid, NULL, &write_to_dpu, &a);
  pthread_join(pid, NULL);

  DPU_FOREACH(set, dpu) {
    dpu_log_read(dpu, stdout);
  }
  dpu_free(set);
}
