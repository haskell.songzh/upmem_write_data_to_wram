#include <stdio.h>
#include <chrono>
#include <stdlib.h>
#include <cstdio>
using namespace std;
using namespace std::chrono;

extern "C" {
#include <dpu.h>
}

#define ARR_SIZE 8192
uint32_t a[ARR_SIZE]; // WRAM is 64KB in total

#ifndef SIZE
#define SIZE (256)
#endif

void init_array() {
  srand(192947);
  for(int i = 0; i < ARR_SIZE; i++) {
    a[i] = rand();
  }
}

#define DPU_BINARY "dpu"

int main() {
  init_array();
  struct dpu_set_t set, dpu;
  dpu_alloc(1, NULL, &set);
  dpu_load(set, DPU_BINARY, NULL);

    for(int i = 0; i < 6; i++){

      auto start_time = high_resolution_clock::now();
      DPU_FOREACH(set, dpu) {
        DPU_ASSERT(dpu_broadcast_to(dpu, "a", 0, a, 4 * SIZE * (1 << i), DPU_XFER_DEFAULT));
      }
      auto end_time = high_resolution_clock::now();
      auto dur = duration_cast<duration<double>>(end_time - start_time);
      printf("copy time of size %2dKB: %f\n", 4 * SIZE * (1 << i) / 1000, dur);
  }
}
