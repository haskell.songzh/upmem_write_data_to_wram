#include <mram.h>
#include <stdint.h>
#include <stdio.h>
__host uint32_t a[8192]; // alloc 8K bytes

int main() {
    printf("address of a: %p\n", a);
    printf("dpu a[0]=%d\n", a[0]);
    a[0] = 111;
    printf("dpu a[0]=%d\n", a[0]);
    return 0;
}
